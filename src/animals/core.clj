(ns animals.core
    (:require [clojure.string :as s]))

(defn read-line-msg
  "Пишет в вывод msg и считывает строку введенную пользователем.
  Если введенная строка пустая, то перезапускает себя.
  Возвращает не пустую строку."
  [msg]
  (println msg)
  (print "> ")
  (flush)
  (let [line (read-line)]
    (if (empty? (s/trim line)) (read-line-msg msg) line)))

(defn read-yes-no
  "Пишет сообщение msg и ожидает ввода пользователем слов yes/no/да/нет.
  Перезапускает себя если введен любой отличный от текст.
  Возвращает yes-fun если пользователь написал да, no-fun если нет."
  [msg yes-fun no-fun]
  (let [line (s/lower-case (read-line-msg msg))]
    (cond (contains? #{"yes" "да"} line) yes-fun
          (contains? #{"no" "нет"} line)  no-fun
          :else       (read-yes-no msg yes-fun no-fun))))

(defn build-node
  "Рекурсивно строит дерево из ответов.
  Объект new-node это дерево из hash-map, которое добавляется в ветку первого элемента из списка answer-stack.
  В answer-stack находится список узлов по которым пришел пользователь.
  в одном элементе answer-stack находятся метка правый/левый и узел из которого пришел пользователь.
  Возращает дерево."
  [new-node answer-stack]
  (let [leaf (:answer (first answer-stack))
        node (:node (first answer-stack))]
    (cond (empty? answer-stack) new-node
          :else (recur (assoc node leaf new-node) (rest answer-stack)))))

(defn add-new-animal
  "Заводит новую запись в дереве животных.
  В answer-stack находятся ответы пользователя.
  Строятся сразу два узла: первый узел с вопросом, и его левая ветка это второй узел с животным.
  Возращает дерево."
  [answer-stack]
  (let [animal (read-line-msg "Напишите название животного, которое вы загадали: ")
        question (read-line-msg "Напишите вопрос, на который можно ответить \"Да\" для загаданного животного: ")]
    (build-node
      {:text question :type 'question :left {:text animal :type 'animal}}
      answer-stack)))

(defn animal?
  "Вовращает true если в node указано животное"
  [node]
  (= (:type node) 'animal))

(defn question?
  "Вовращает true если node это вопрос"
  [node]
  (= (:type node) 'question))

(defn make-question
  "Вовращает строку с вопросом в зависимости от типа узла (животное/вопрос)"
  [node]
  (if (animal? node)
    (str "Это " (s/lower-case (:text node)) "?")
    (str (s/capitalize (:text node)))))

(defn win
  "Победное сообщение! Вовращает последний элемент из списка ответов.
  В последнем элементе всегда хранится начальное дерево животных.
  Т.е измения дерева не происходит."
  [answer-stack]
  (println "Отлично! Компьютер угадал ваше животное.")
  (:node (last answer-stack)))

(defn lose
  "Компьютер не угадал животное и просит добавить его в дерево.
  Возвращает дерево с новым элементом."
  [answer-stack]
  (println "Эх, Компьютер не смог отгадать.")
  (add-new-animal answer-stack))

(defn turn
  "Пишет вопрос. Спрашивает ответ да/нет. Смотрит по дереву есть ли ветки.
  Если ветка есть, то переходит по ней, записав левый/правый и само дерево в answer-stack.
  Если ветвей нет, и тип узла животное и ответили да - то победа!
  Иначе проигрыш и надо спросить про новое животное.
  Возвращает дерево животных, с новым или без животным."
  ([node answer-stack]
  (let [answer (read-yes-no (make-question node) :left :right)
        next-node (answer node)
        updated-stack (conj answer-stack {:answer answer :node node})]
    (cond next-node (recur next-node updated-stack)
          (and (= answer :left) (animal? node)) (win updated-stack)
          :else (lose updated-stack))))
  ([node]
   (if (empty? node)
     (add-new-animal ())
     (turn node ()))))

(defn load-animals
  "Считать объект животных с файла и десериализовать его."
  [filename]
  (try (let [node (read-string (slurp filename))]
         (if (map? node) node (throw Exception)))
       (catch Exception e (let []
                            (println (str "Не могу прочитать файл" filename)) {}))))

(defn save-animals
  "Сериализовать и записать объект животных в файл."
  [filename node]
  (try (spit filename node)
       (catch Exception e (println (str "Не могу записать в файл " filename)))))

(defn start
  "Главный цикл игры. При каждом завершении раунда записывает дерево в файл.
  Потом спрашивает продолжать ли игру."
  [node filename]
  (if (not-empty node)
    (println "Загадайте животное, Компьютер попробует его угадать. Отвечайте да/нет/yes/no.")
    (println "Похоже база животных пуста. Давайте внесем туда первое животное."))
  (let [new-node (turn node)]
    (save-animals filename new-node)
    (if (read-yes-no "Хотите еще сыграть?" true false) (recur new-node filename))))

(defn -main
  "Главная точка входа в программу. Считывает дерево из указанного файла."
  ([f & args]
   (start (load-animals f) f))
  ([]
   (-main "animals.db")))
